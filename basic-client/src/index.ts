import { Client } from "@elastic/elasticsearch";
import * as data from "../../data/Posts.json";
const bootstrap = async () => {
  const client = new Client({
    node: "http://localhost:9200",
    auth: { username: "elastic", password: "changeme" }
  });
  try {
    //await client.indices.create({ index: "posts" });

    const chunkArray = (myArray: any[], chunk_size: number) => {
      const results = [];

      while (myArray.length) {
        results.push(myArray.splice(0, chunk_size));
      }

      return results;
    };

    const body: any[] = (data as any).posts.row.reduce((a: any, b: any) => {
      a.push({ index: { _index: "posts" }, ...b });
      return a;
    }, []);

    const chunks = chunkArray(body, Math.ceil(body.length / 20));

    for (let i = 0; i < chunks.length; i++) {
      await client.bulk({ index: "posts", body: chunks[i] });
    }

    /* const { body } = await client.search({
      index: "tags",
      body: {
        query: {
          match: {
            "@WikiPostId": "127169"
          }
        }
      }
    }); */
    //console.log(body.hits.hits[0]._source);
  } catch (error) {
    console.log(error);
  }
};

bootstrap();
