import {
  DataSearch,
  MultiList,
  ReactiveBase,
  ReactiveList,
  ResultCard,
  SelectedFilters
} from "@appbaseio/reactivesearch";
import React from "react";
import { BrowserRouter, Route, Switch, useHistory } from "react-router-dom";
import { DetailView } from "./components/DetailView";
import { transformResponse } from "./util";

function RouterBase() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <App />
        </Route>
        <Route path="/:id">
          <DetailView />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

function App() {
  const history = useHistory();
  return (
    <ReactiveBase
      app="posts"
      url="http://localhost:9200"
      credentials="elastic:changeme"
    >
      <DataSearch
        componentId="postsSearch"
        dataField={["@Body", "@Tags"]}
        placeholder="Search for a Question"
        className="search"
        title="Volltextsuche"
        debounce={500}
        fuzziness={2}
        URLParams={true}
        queryFormat="and"
        react={{
          and: "tagsFilter"
        }}
      />
      <div className="grid-container">
        <div className="filter">
          <MultiList
            componentId="tagsFilter"
            dataField="@Tags.keyword"
            title="Tagsearch"
            loader="Loading..."
            react={{
              and: "postsSearch"
            }}
            URLParams={true}
            queryFormat="or"
          />
          <SelectedFilters />
        </div>
        <div className="result">
          <ReactiveList
            componentId="TagsResult"
            dataField="@Body"
            react={{
              and: ["postsSearch", "tagsFilter"]
            }}
            size={10}
          >
            {({ data }) => (
              <ReactiveList.ResultCardsWrapper>
                {data.map(item => {
                  item = transformResponse(item);
                  return (
                    <ResultCard
                      onClick={() => history.push(`/${item.id}`)}
                      key={item.id}
                      className="result-card-wrapper"
                    >
                      <ResultCard.Title
                        dangerouslySetInnerHTML={{ __html: item.title }}
                      />
                      <ResultCard.Description
                        dangerouslySetInnerHTML={{
                          __html: item.body.replace(
                            new RegExp(/<pre>/, "g"),
                            '<pre class="prettyprint">'
                          )
                        }}
                        style={{
                          overflow: "auto"
                        }}
                      />
                    </ResultCard>
                  );
                })}
              </ReactiveList.ResultCardsWrapper>
            )}
          </ReactiveList>
        </div>
      </div>
    </ReactiveBase>
  );
}

export default RouterBase;
