import React from "react";
import { Link, useParams } from "react-router-dom";
import { transformResponse } from "../util";

const formatter = new Intl.DateTimeFormat("de");

export function DetailView() {
  const [item, setItem] = React.useState({});
  const [loading, setLoading] = React.useState(true);
  const { id } = useParams();
  React.useEffect(() => {
    async function fetchData() {
      const i = await fetch("http://localhost:9200/posts/_search", {
        method: "POST",
        body: JSON.stringify({
          query: {
            match: {
              "@Id": id
            }
          }
        }),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Basic ${btoa("elastic:changeme")}`
        }
      })
        .then(r => r.json())
        .then(r => r.hits.hits[0]);

      setItem(transformResponse(i._source));
      setLoading(false);
    }
    fetchData();
  }, [id]);
  return !loading ? (
    <>
      <Link to="/">Zurück</Link>
      <h3>{item.title}</h3>
      <div>{JSON.stringify(item)}</div>
      <div className="metadata">
        <div className="viewed">
          Question was <span>{item.viewcount}</span> times viewed
        </div>
        <div>
          Created At:
          {item.creationdate
            ? formatter.format(new Date(item.creationdate))
            : "N/A"}
        </div>
        <div>
          Last Edited:
          {item.lasteditdate
            ? formatter.format(new Date(item.lasteditdate))
            : "N/A"}
        </div>
        <div>
          Last Activity:
          {item.lastactivitydate
            ? formatter.format(new Date(item.lastactivitydate))
            : "N/A"}
        </div>
      </div>
      <div
        className="content-wrapper"
        dangerouslySetInnerHTML={{
          __html: item.body.replace(
            new RegExp(/<pre>/, "g"),
            '<pre class="prettyprint">'
          )
        }}
      ></div>
    </>
  ) : (
    <h3>Loading …</h3>
  );
}
