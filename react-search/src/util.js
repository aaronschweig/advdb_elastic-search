export function transformResponse(obj) {
  Object.keys(obj).forEach(k => {
    obj[k.replace("@", "").toLowerCase()] = obj[k];
    delete obj[k];
  });
  return obj;
}
