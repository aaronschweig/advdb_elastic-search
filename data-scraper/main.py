import json

from bs4 import BeautifulSoup
from requests import get

baseUrl = "https://archive.org/download/stackexchange/"
downloadLinks = []

data = get(baseUrl).text

soup = BeautifulSoup(data, "html.parser")

table = soup.find(class_="directory-listing-table")


for a in table.findAll("a"):
    if ".7z" in a["href"]:
        downloadLinks.append(baseUrl+a["href"])


with open('links.json', 'w') as file:
    json.dump(downloadLinks, file)

print(len(downloadLinks))
