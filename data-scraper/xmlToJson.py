import json
import os

import xmltodict

locky = True
path = "../data/"

globalWriteFile = None


def openFileStream(filename: str):
  globalWriteFile = open(filename, 'r')  

def callback(parent, child):
    if locky:
        print(child)
        lock = False
    return True

with open(path + 'Posts.xml') as f:
    x = xmltodict.parse(f.read())
    with open(path + 'Posts.json', 'w') as of:
        json.dump(x, of)

""" for filename in os.listdir(path):
    if filename.endswith('.xml'):
        print(filename)
        x = xmltodict.parse(open(path + filename).read(), item_depth=1, item_callback=callback)
        filename = filename.replace('.xml', '') """
