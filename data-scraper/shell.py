import json
import os

if not os.path.exists('../../data'):
    os.makedirs('../../data')

with open('links.json') as file:
    data = json.load(file)

    for link in data:
        os.system('wget ' + link + ' -P ../data/')
