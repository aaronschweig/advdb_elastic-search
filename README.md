﻿﻿﻿﻿# Advanced Databases - Elastic Search
   
#### Future Stuff
* XML Processing wäre mit Apache Spark deutlich effizienter und schneller
* Evtl. Alle XML Dateien durch spark umwandeln und mithilfe von Batching in ES? Schneller, effizienter. 

## Step-By-Step Guide zum Ausführen des Systems (lokal)

### Lokales Cluster

1. Mithilfe von `docker --version` sicherstellen, dass Docker installiert ist

   ```
   docker --version
   Docker version 19.03.2, build 6a30dfc
   ```

2. Das Repository (wenn nicht bereits geschehen klonen) mithilfe von

   ```
   git clone https://aaronschweig@bitbucket.org/aaronschweig/advdb_elastic-search.git
   ```

klonen. Sollte bereits geklont sein, mithilfe von `git pull` sicherstellen dass der neuste Codestand vorhanden ist.

### Alternative a:
3. Nun kann mittels

   ```
   docker swarm init
   ```

   ein lokales Cluster initalisiert werden.

4. Im Anschluss wird mit dem Befehl

   ```
   docker stack deploy -c docker-compose.yml elk-stack
   ```

   die lokale Umgebung erstellt.

### Alternative b:

   ```
   docker-compose up
   ```

Es werden nun 2 Elasticsearchinstanzen, 1 Kibane-Instanz, sowie eine Metricbeatinstanz gestartet. Über http://localhost:5601 kann nun auf das lokale Cluster zugegriffen werden

### Remote Cluster (auf dem Uni-Server)

Um auf den Uniserver zugreifen zu können ist weniger Aufwand notwendig. Allerdings muss hierbei beachtet werden, dass **keine** Datenveränderungen oder Löschungen vorgenommen werden dürfe, da es sich dabei um Produktivdaten handelt.

1. Mithilfe dieses Commands wird ein SSH-Port-Fowarding genutzt um auf den entfernten Server zugreifen zu können.

```
ssh ubuntu@141.72.191.135 -L 5601:localhost:5601
```

> Sollte hier ein berechtigungsproblem auftreden muss evtl mit der `-i`-Flag der Pfad zum richtigen SSH-Key angegeben werden.

2. Nun ist auf http://localhost:5601 die **produktive** Kibana- und Elasticsearchinstanz verfügbar.

## Nutzung von Metricbeat

Metricbeat sammelt allgemeine Daten zu dem aktuellen Cluster und spiegelt die Performance dieses wieder. Dazu muss ein zusätzlicher Schritt ausgeführt werden.

// TODO

## Todos:

- Docker-compose file
  - Kann der Server exposed werden --> Kibana von außen? --> ssh tunnel
  - Elasticsearch mit mehr Power austatten und verclustern
- Nur 40GB, benötigen deutlich mehr!

## Aufbau

```
.
├── README.md
├── basic-client
│   ├── package.json
│   ├── src
│   │   └── index.ts
│   ├── tsconfig.json
│   └── yarn.lock
├── config
│   └── elasticsearch.yml
├── data-scraper
│   ├── Pipfile
│   ├── Pipfile.lock
│   ├── links.json
│   ├── main.py
│   └── shell.py
└── docker-compose.yml
```

Die Daten sollten eigentlich mithilfe von [Kaggle](https://www.kaggle.com/) heruntergeladen und auf dem Server verfügbar gemacht werden. Leider war es aufgrund der Größe nicht möglich den Datensatz herunterzuladen.
Stattdessen bestand die kostenpflichitge Möglichkeit mithilfe der _Google BigQuery-API_ die Daten zu lesen. Allerdings fand sich in der Beschreibung des Datensatzes in Link zur [Datenquelle](https://archive.org/details/stackexchange).
Die Website blockierte aufrund der Größe von über 60GB den direkten Download, weswegen ein manueller Download aller dateien einzeln stattfinden muss. Es konnten mithilfe eines kleinen Skriptes (`data-scraper`) alle 353 Downloadlinks herausgefunden werden, welche danach gleichzeitig heruntergeladen werden können.

## Setup

Zur Vereinfachung des Konfigurationsvorgangs wird [Docker](https://www.docker.com/) genutzt. Innerhalb der `docker-compose.yml`-Datei wird jeder Service definiert und Konfiguriert, sodass am Ende mithilfe eines Commands eine gesamte Infrastruktur ertsellt werden kann.

## Notes

- Replizieren läuft mit `docker service scale <id>=2`und wird direkt mit ins cluster aufgenommen

* Sollte auch automatisch alle Daten Replizieren?? --> Brauchen deutlich mehr als 4 GB Arbeitsspeicher
* WIP Dockerfile, muss noch alles in die KONFIGS auslagern
* Metricbeat als nettes Add-On was Elastic sonst noch so kann
